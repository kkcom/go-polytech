class Offer < ActiveRecord::Base
  belongs_to :user
  has_many :replies

  acts_as_paranoid

  validates :content,
      presence: true,
      length: { maximum: 140 }
end
