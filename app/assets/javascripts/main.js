var ready = function(){
  $(document).load($(window).bind("resize", checkPosition));

  function checkPosition() {
      if (window.matchMedia('(min-width: 543px)').matches) {
        // if nav list was open in mobile version, close it in desktop-nav
        // so the menu is not dublicated
        $(".navbar-collapse").removeClass("in");
      }
  }

  function clampLines(){
  	// break words on replies and limit to 2 lines with added dots after last line
  	$('.offer-content[data-behavior~=clamp]').each(function(index, element) {
    	$clamp(element, { clamp: 2, useNativeClamp: false });
  	});	
  }

  clampLines();
};

$(document).ready(ready);
$(document).on('turbolinks:load', ready);
