$(document).on "click", "[data-behavior~=choose-replies-li]", ->
	if not $(this).hasClass("active")
		$(this).find("[data-behavior~=choose-replies]").trigger("click")

$(document).on "click", "[data-behavior~=read-replies]", ->
	$(this).find("[data-behavior~=choose-replies]").trigger("click")

$(document).on 'ready page:load', ->
	setTimeout ( ->
		$("[data-behavior~=read-replies]").trigger("click")
	), 1000