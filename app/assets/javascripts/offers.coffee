$(document).on 'ready page:load', ->
	console.log "hi"
	$('textarea').autosize()

$(document).on "click", "[data-behavior~=reply-button]", ->
	event.preventDefault()
	offerId = $(this).data("id")

	createdAt = $(".offer-created-at[data-id=#{offerId}]")
	window.createdAtBottomMargin = createdAt.css("margin-bottom")
	createdAt.css("margin-bottom", "0")

	$("[data-id=#{offerId}][data-behavior~='offer-reply-block']").slideDown("fast")	
	$(this).hide()

$(document).on "click", "[data-behavior~=cancel-reply-button]", ->
	offerId = $(this).data("id")
	
	createdAt = $(".offer-created-at[data-id=#{offerId}]")
	createdAt.css("margin-bottom", window.createdAtBottomMargin)

	$("[data-id=#{offerId}][data-behavior~='offer-reply-block']").slideUp("fast")
	$("[data-id=#{offerId}][data-behavior~='reply-button']").show()

$(document).on "submit", ".offer .new_reply", ->
	$(this).find("input[type=submit]").val("Отправка...")