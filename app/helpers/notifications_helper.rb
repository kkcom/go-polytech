module NotificationsHelper
    def new_replies
      Reply.joins(:offer).merge(Offer.where(user: current_user)).merge(Reply.where(seen: nil)).count
    end

	def new_replies_offer offer
      offer.replies.where(seen: nil).count
    end
end
