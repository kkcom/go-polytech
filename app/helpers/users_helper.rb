module UsersHelper
    def current_user_avatar
        user_hash = vk_api.users.get(user_ids: current_user.uid, fields: 'photo_100', https: 1)[0]

        image_tag(user_hash[:photo_100], id: "nav-avatar", class: "img-circle")
    end
    def avatar(user)
        user_hash = vk_api.users.get(user_ids: user.uid, fields: 'photo_100', https: 1)[0]

        link_to(avatars_user_path(user), remote: true, class: "media-left") do
            image_tag(user_hash[:photo_100], class: 'media-object avatar', alt: "Avatar")
        end
    end
    def user_link(user)
        user_hash = vk_api.users.get(user_ids: user.uid, fields: 'domain', lang: 0)[0]

        link_to user_hash[:first_name] + ' ' + user_hash[:last_name], "https://vk.com/" + user_hash[:domain]
    end
end
