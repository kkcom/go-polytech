module TimeHelper
    def current_time timestamp
      timestamp + 3.hours
    end

    def pretty_time timestamp
      timestamp = current_time timestamp
      
      if Time.now - timestamp < 5.minutes
        t('recently')
      elsif timestamp.to_date == Date.today
        l(timestamp, format: :time_only)
      elsif timestamp.to_date == Date.yesterday
        t('yesterday') + ", " + l(timestamp, format: :time_only )
      elsif timestamp.to_date.year == Date.today.year
        l(timestamp, format: :short)
      else
        l timestamp, format: :short_with_year
      end
    end
end
