class OffersController < ApplicationController
  before_action :require_auth, except: [:index, :about]

  def index
    @offers = Offer.where.not(accepted: nil).order('created_at DESC')
  end

  def create
    @offer = current_user.offers.create(offer_params).save
    redirect_to offers_path
  end

  def destroy
    @offer = Offer.find(params[:id])
    @offer.destroy
  end

  def mine
    @offers = current_user.offers.order('created_at DESC')
  end

  def about
  end

  private
    def offer_params
      params.require(:offer).permit(:content)
    end
end
