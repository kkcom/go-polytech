class RepliesController < ApplicationController
  before_action :require_auth

  def index
    @offers = current_user.offers.order('created_at DESC')
    @offer = @offers.first
    @replies = @offer ? @offer.replies.order('created_at DESC') : []
  end

  def create
    @offer = Offer.find(params[:offer_id])
    @reply = @offer.replies.create(reply_params).save
  end

  def destroy
    @reply = Reply.find(params[:id])
    @offer = @reply.offer
    if @reply.user == current_user
      @reply.destroy
    end
  end

  def choose
    @offers = current_user.offers.order('created_at DESC')
    @offer = Offer.find(params[:offer_id])
    @replies = @offer ? @offer.replies.order('created_at DESC') : []
  end

  def read
    @offers = current_user.offers.order('created_at DESC')
    @offer = Offer.find(params[:offer_id]) 
    ids = params[:ids]
    @replies = Reply.where(id: ids).order('created_at DESC')
    @replies.each do |reply|
      reply.update(seen: Time.now) if reply.offer.user == current_user
    end
  end

  private
    def reply_params
      params.require(:reply).permit(:message).merge(user: current_user)
    end
end
