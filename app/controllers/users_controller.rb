class UsersController < ApplicationController

    def avatars
        photos_hash = vk_api.photos.get(
                owner_id: User.find(params[:id]).uid,
                album_id: "profile",
                rev: true
                )
        @photos = []
        photos_hash.each do |photo|
            @photos.push photo[:src_big]
        end
    end
end
