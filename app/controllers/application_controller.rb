class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user
  helper_method :requires_auth
  helper_method :vk_api

  private
    def current_user
        @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end

    def require_auth
        redirect_to root_path if not current_user
    end

    def vk_api
        @vk_api = VkontakteApi::Client.new
    end
end
