class Admin::OffersController < AdminController
	before_action :require_auth, :require_admin

	def index
		@offers = Offer.all.order('created_at DESC')
	end

	def create
		uid = params[:user_uid]
		if uid.present?
			user = User.where(uid: uid).first
			if not user.present?
				user = User.create!(uid: uid, provider: 'vkontakte')
			end	
			@offer = user.offers.create(offer_params)
		else
			@offer = current_user.offers.create(offer_params)
		end
	    redirect_to admin_offers_path
	end

	def destroy
		@offer = Offer.find(params[:id])
		@offer.destroy
	end

	def accept
		@offer = Offer.find(params[:id])
		@offer.update(accepted: Time.now)
	end

	private
	    def offer_params
			params.require(:offer).permit(:content)
	    end
end