class AddNewToReplies < ActiveRecord::Migration
  def change
    add_column :replies, :new, :timestamp
  end
end
