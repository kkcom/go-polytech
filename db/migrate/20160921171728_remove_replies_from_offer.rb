class RemoveRepliesFromOffer < ActiveRecord::Migration
  def change
    remove_column :offers, :replies
  end
end
