class RenameNewInReplies < ActiveRecord::Migration
  def change
  	rename_column :replies, :new, :seen
  end
end
