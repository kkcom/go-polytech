class AddRepliesToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :replies, :text, array:true, default: []
  end
end
