class AddAcceptedToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :accepted, :timestamp
  end
end
