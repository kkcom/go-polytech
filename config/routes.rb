Rails.application.routes.draw do
    root 'offers#index'

    get "/auth/:provider/callback" => "sessions#create"
    get "/signout" => "sessions#destroy"
    get "/about" => "offers#about"

    resources :offers do
        get 'mine', on: :collection

        resources :replies, only: [:create, :destroy]
    end

    resources :replies, only: [:index] do
        get 'choose', on: :collection
        patch 'read', on: :collection
    end

    resources :users, only: [:avatars] do
        get 'avatars', on: :member
    end

    namespace :admin do
        root :to => "offers#index"
        resources :offers do
            patch 'accept', on: :member
        end
    end
end
