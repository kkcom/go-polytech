VkontakteApi.configure do |config|
  # Authorization parameters (not needed when using an external authorization):
  # config.app_id       = ENV['vk_api_key']
  # config.app_secret   = ENV['vk_api_secret']
  # config.redirect_uri = root_url + '/auth/vkontakte/callback'

  # Faraday adapter to make requests with:
  config.adapter = :net_http

  # Logging parameters:
  # log everything through the rails logger
  config.logger = Rails.logger

  config.faraday_options = {
    ssl: {
      :ca_file => '/usr/lib/ssl/certs/ca-certificates.crt'
    }
  }
  # log requests' URLs
  # config.log_requests = true

  # log response JSON after errors
  # config.log_errors = true

  # log response JSON after successful responses
  # config.log_responses = false
end
